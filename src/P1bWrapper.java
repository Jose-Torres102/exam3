import java.util.ArrayList;
import java.util.List;




public class P1bWrapper {
	public static interface Position<E> {
		
		public E getValue();

	}

	public static class BinaryTreeNode<E> implements Position<E> {

		private E value;
		private BinaryTreeNode<E> leftChild;
		private BinaryTreeNode<E> rightChild;
		private BinaryTreeNode<E> parent;

		

		public BinaryTreeNode(E value) {
			super();
			this.value = value;
			this.leftChild = null;
			this.rightChild = null;
			this.parent = null;

		}

		
		public BinaryTreeNode(E value, BinaryTreeNode<E> parent, BinaryTreeNode<E> leftChild, BinaryTreeNode<E> rightChild) {
			super();
			this.value = value;
			this.leftChild = leftChild;
			this.rightChild = rightChild;
			this.parent = parent;
		}

		@Override
		public E getValue() {
			return this.value;

		}


		public BinaryTreeNode<E> getLeftChild() {
			return leftChild;
		}


		public void setLeftChild(BinaryTreeNode<E> leftChild) {
			this.leftChild = leftChild;
		}


		public BinaryTreeNode<E> getRightChild() {
			return rightChild;
		}


		public void setRightChild(BinaryTreeNode<E> rightChild) {
			this.rightChild = rightChild;
		}


		public void setValue(E value) {
			this.value = value;
		}


		public BinaryTreeNode<E> getParent() {
			return parent;
		}


		public void setParent(BinaryTreeNode<E> parent) {
			this.parent = parent;
		}

	}

	public static interface SimpleBinaryTree<E> {

		// get tree root
		public Position<E> root();
		
		// get left child of node
		public Position<E> left(Position<E> p);
		
		// get right child of node
		public Position<E> right(Position<E> p);
		
		// get sibling
		public Position<E> sibling(Position<E> p);

		// 
		public boolean isEmpty();
		
		// Get all the nodes in the path from root to node with value e
		// return empty list if node with valeu e is not  found.
		public List<Position<E>> getPath(E e);

		// return size of the tree
		public int size(); 
		
		public List<Position<E>> descendants(E e);
		public int depth(E e);


	}	

	public static class SimpleBinaryTreeImp<E> implements SimpleBinaryTree<E> {
		
		private BinaryTreeNode<E> root;
		

		
		public SimpleBinaryTreeImp(BinaryTreeNode<E> root) {
			super();
			this.root = root;
		}
		
		public SimpleBinaryTreeImp(BinaryTreeNode<E> root, 
				SimpleBinaryTree<E> T1, SimpleBinaryTree<E> T2) {
			super();
			this.root = root;
			if (T1 != null) {
				BinaryTreeNode<E> temp = (BinaryTreeNode<E>)T1.root();
				this.root.setLeftChild(temp);
				temp.setParent(this.root);
				
			}
			if (T2 != null) {
				BinaryTreeNode<E> temp = (BinaryTreeNode<E>)T2.root();

				this.root.setRightChild(temp);
				temp.setParent(this.root);

			}

		}



		@Override
		public Position<E> root() {
			return this.root;
		}


		private void check(Position<E> p) {
			if (p==null) {
				throw new IllegalArgumentException();
			}
		}
		@Override
		public Position<E> left(Position<E> p) {
			this.check(p);
			BinaryTreeNode<E> temp = (BinaryTreeNode<E>)p;
			return temp.getLeftChild();
		}


		@Override
		public Position<E> right(Position<E> p) {
			this.check(p);
			BinaryTreeNode<E> temp = (BinaryTreeNode<E>)p;
			return temp.getRightChild();

		}

		@Override
		public Position<E> sibling(Position<E> p) {
			this.check(p);
			BinaryTreeNode<E> temp = (BinaryTreeNode<E>)p;
			if (temp.getParent().getLeftChild() != temp) {
				return temp.getRightChild();
			}
			else {
				return temp.getLeftChild();
			}

		}
		
		@Override
		public boolean isEmpty() {
			return this.size() == 0;
		}

		@Override
		public List<Position<E>> getPath(E e) {
			List<Position<E>> L = new ArrayList<Position<E>>();
			// DO NOT WORK ON THIS METHOD
			return null;
			
		}
		

	
		@Override
		public int size() {
			// DO NOT WORK ON THIS METHOD

			return -1;
		}

		
		//////////////////////////////////////////////////
		
		public void print() {
			this.printAux(this.root, 0);
		}

		private void printAux(BinaryTreeNode<E> N, int i) {
			if (N != null) {
				this.printAux(N.getRightChild(), i + 4);
				for (int j=0; j < i; ++j) {
					System.out.print(" ");
				}
				System.out.println(N.getValue());
				this.printAux(N.getLeftChild(), i + 4);
			}
			
		}
		
		//////////////////////////////////////////////////
		// FOR STUDENTS
		// Find the positions for all the descendants of e. Assume no duplicates
		// Returns a list with e  and all of its descendants in pre-order. 
		// Returns empty list if e is not found.

		@Override
		public List<Position<E>> descendants(E e) {
			List<Position<E>> L = new ArrayList<Position<E>>();
			List<Position<E>> list = new ArrayList<Position<E>>();
			fillArrayList(this.root(), list);
			Position<E> element = this.findElement(e, list);
			if(element != null) fillArrayList(element, L);
			return L;
			
		}
		
		private void fillArrayList(Position<E> n, List<Position<E>> l) {
			if(n == null) return;
			l.add(n);
			fillArrayList(left(n), l);
			fillArrayList(right(n), l);
		}
		

		private Position<E> findElement(E e, List<Position<E>> list ) {
			for (Position<E> p : list) {
				if(p.getValue().equals(e)) return p;
			}
			return null;
		}

		//////////////////////////////////////////////////
		// FOR STUDENTS
		// Find the depth for a node with value e. Assume no duplicates
		// Returns the depth. Note: If e is the root, the depth is 0. 
		// Returns -1 if e is not found in the tree.
		@Override
		public int depth(E e) {
			if (this.root() == null) return -1;
			if(this.root().getValue().equals(e)) return 0;
			return findDepth(this.root(), e, 1) - 1;
		}

		private int findDepth(Position<E> p, E e, int i) {
			if(p == null) return 0;
			if(p.getValue().equals(e)) return i; 
		
				return this.findDepth(this.right(p), e, i+1) + this.findDepth(this.left(p), e, i+1);
			}
		}




	}
